#include <iostream>
#include <cassert>

using namespace std;

template<typename T, int N>
constexpr size_t array_size(T (&arr)[N]) {
    return N;
}

template<typename T, int N>
constexpr T *array_end(T (&arr)[N]) {
    return arr + N;
}

int const *find_value(int const *begin, int const *end, int value) {
    while(begin < end && *begin != value)
        ++begin;
    return begin;
}

void swap(int *x, int *y) {
    int t = *x;
    *x = *y;
    *y = t;
}

void reverse(int *begin, int *end) {
    while(begin != end && begin != --end)
        swap(begin++, end);
}

int const * binary_find(int const *begin, int const *end, int value) {
    int const *b = begin;
    int const *e = end;

    while(b < e) {
        int const *mid = b + (e - b) / 2;

        if (*mid > value)
          e = mid;
        else if(*mid < value)
            b = mid + 1;
        else
        return mid;
    }

    // did not find the element, so return end
    return end;
}

template<typename T, int N>
struct StaticStack {
            bool is_full()const {
                return top == N;
            }
            bool is_empty()const {
                return top == 0;
            }
            T const& peek()const {
                return arr[top - 1];
            }

            T& pop() {
                return arr[--top];
            }
            StaticStack<T, N>& push(T const& e) {
                assert(top < N);
                arr[top++] = e;
                return *this;
            }

            const T *begin()const {
                return arr;
            }

            const T *end()const {
                return array_end(arr);
            }

private:
    T arr[N];
    int top;
        };


int main() {

    {
        int arr[] = {1, 2, 3, 4, 5, 6};

        assert(array_size(arr) == 6);

        int const *end = array_end(arr);

        assert(find_value(arr, end, 1) == arr);
        assert(find_value(arr, end, 6) == arr + 5);
        assert(find_value(arr, end, 3) == arr + 2);
        assert(find_value(arr, end, 30) == end);
        assert(find_value(arr, end, 0) == end);

        assert(binary_find(arr, end, 1) == arr);
        assert(binary_find(arr, end, 6) == arr + 5);
        assert(binary_find(arr, end, 3) == arr + 2);
        assert(binary_find(arr, end, 30) == end);
        assert(binary_find(arr, end, 0) == end);
    }

    {
        int reversed[] = {4, 3, 2, 1, 0};
        reverse(reversed, array_end(reversed));

        for (auto i = 0; i < array_size(reversed); ++i)
            assert(i == reversed[i]);
    }

    {
        int reversed[] = {5, 4, 3, 2, 1, 0};
        reverse(reversed, array_end(reversed));

        for (auto i = 0; i < array_size(reversed); ++i)
            assert(i == reversed[i]);
    }


    {
        StaticStack<string, 3> stack;
        assert(stack.is_empty());

        assert("hello" == stack.push("hello").peek());
        assert("world" == stack.push("world").peek());
        assert("universe" == stack.push("universe").peek());
        assert(stack.is_full());

        assert(stack.pop() == "universe");
        assert(stack.pop() == "world");
        assert(stack.pop() == "hello");
        assert(stack.is_empty());
    }

    {
        StaticStack<int, 5> stack;
        stack.push(0).push(1).push(2).push(3).push(4);

        assert(find_value(stack.begin(), stack.end(), 0) == stack.begin());
        assert(find_value(stack.begin(), stack.end(), 2) == stack.begin() + 2);
        assert(find_value(stack.begin(), stack.end(), 10) == stack.end());

        assert(binary_find(stack.begin(), stack.end(), 2) == stack.begin() + 2);
    }

    return 0;
}
